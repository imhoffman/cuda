## CUDA programming

My experiments on my GPU. Here is what I am working with:

```
$ nvcc -ccbin=gcc-9 C/test_queries.cu && cuda-memcheck ./a.out 
========= CUDA-MEMCHECK

           CUDA version:   11.0
         driver version:  450.66
           nvcc version: 11.0.221
 ---------------------------------------

 =Properties of Device 0 (PCI bus ID: 1)=
 GeForce GTX 1650 SUPER (single-GPU board)
 Compute Capability: 7.5

             Clock Rate:  1.75 GHz
      Memory Clock Rate:  6.00 GHz
       Memory Bus Width:   128 bits
 Total (non-ECC) Memory:  3903 MB
          L2 Cache size:  1024 kB

    20 multiprocessors
     3 asynchronous engines
  1024 threads per block (max)
  1024 threads per multiprocessor (max)

 single-to-double performance ratio: 32

========= ERROR SUMMARY: 0 errors
```

```
$ nvfortran -cuda fortran/test_queries.f90 && cuda-memcheck ./a.out 
========= CUDA-MEMCHECK

 =Properties of Device 0 (PCI bus ID: 1)=
 GeForce GTX 1650 SUPER (single-GPU board)
 Compute Capability: 7.5

             Clock Rate:  1.75 GHz
      Memory Clock Rate:  6.00 GHz
       Memory Bus Width:   128 bits
 Total (non-ECC) Memory:  3903 MB
          L2 Cache size:  1024 kB

    20 multiprocessors
     3 asynchronous engines
  1024 threads per block (max)
     0 threads per multiprocessor (max)

 single-to-double performance ratio: 32

========= ERROR SUMMARY: 0 errors
```

And on my old laptop:

```
$ nvcc C/test_queries.cu && cuda-memcheck ./a.out
========= CUDA-MEMCHECK

           CUDA version:   11.0
         driver version:  450.66
           nvcc version: 11.0.221
 ---------------------------------------

 =Properties of Device 0 (PCI bus ID: 4)=
 GeForce 840M (single-GPU board)
 Compute Capability: 5.0

             Clock Rate:  1.12 GHz
      Memory Clock Rate:  0.90 GHz
       Memory Bus Width:    64 bits
 Total (non-ECC) Memory:  2004 MB
          L2 Cache size:  1024 kB

     3 multiprocessors
     1 asynchronous engines
  1024 threads per block (max)
  2048 threads per multiprocessor (max)

 single-to-double performance ratio: 32

========= ERROR SUMMARY: 0 errors
```

