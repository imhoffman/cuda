//
//  nvcc -allow-unsupported-compiler -ccbin gcc-12 regex_queries.cu -l pcre2-8
//  or
//  nvcc -allow-unsupported-compiler -ccbin gcc-12 -c regex_queries.cu -o /tmp/junk.o && nvcc -allow-unsupported-compiler -ccbin gcc-12 /tmp/junk.o /usr/lib/libpcre2-8.a -o gpu_query
//

//  I think this must be done before the header read ?
#define PCRE2_CODE_UNIT_WIDTH 8

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<pcre2.h>
#include<cuda.h>
#include<cuda_runtime_api.h>


//  subroutine to print CUDA core only for failures
//  as per 3.2.9
//  https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#device-enumeration
void
cudaErrorCheck ( void )
{
  int error_code = cudaPeekAtLastError();
  if ( error_code )
    fprintf( stderr, "\n latest CUDA error code: %d\n\n", error_code );
  
  return;
}


//
//  I can't find these queries in the API, so pull
//   some info from the command-line
//  what==1 get driver from /proc
//  what==2 get CUDA from nvidia-smi
//  what==3 get version from nvcc help
//
void
driver_version ( char * driver_parse, const int query_what )
{
  const int buffer_size = 1024;   //  check what the calling scope used on the input addr
  char cmd[buffer_size];
  char buffer[buffer_size];
  FILE *cmd_fp;

  //  PCRE2 code from their pcre2demo.c
  pcre2_code *re;
  PCRE2_SPTR pattern;     /* PCRE2_SPTR is a pointer to unsigned code units of */
  PCRE2_SPTR subject;     /* the appropriate width (in this case, 8 bits). */
  int errornumber;
  int rc;
  PCRE2_SIZE erroroffset;
  PCRE2_SIZE *ovector;
  PCRE2_SIZE subject_length;
  pcre2_match_data *match_data;

  //
  //  match command and regex to desired info
  //
  memset( cmd, 0, (size_t)buffer_size );
  switch( query_what ) {
    case 1:
      strcpy( cmd, "cat /proc/driver/nvidia/version" );
      pattern =(PCRE2_SPTR) "(\\d+)\\.(\\d+)\\.{0,1}(\\d+){0,1}";
      break;
    case 2:
      strcpy( cmd, "nvidia-smi" );
      pattern =(PCRE2_SPTR) "CUDA.*?(\\d+)\\.(\\d+)\\.{0,1}(\\d+){0,1}";
      break;
    case 3:
      strcpy( cmd, "nvcc --version" );
      pattern =(PCRE2_SPTR) ", V(\\d+)\\.(\\d+)\\.{0,1}(\\d+){0,1}";
      break;
    default:
      {};
  }

  //
  //  obtain string input from command-line pipe call
  //
  memset( buffer, 0, (size_t)buffer_size );
  if ( ( cmd_fp = popen( cmd, "r" ) ) != NULL )
    fread( buffer, (size_t)buffer_size, (size_t)1, cmd_fp );
    //fgets( buffer, (size_t)buffer_size, cmd_fp );    //  if only one-line
  //  the command-line info (esp. nvidia-smi) may be longer than the buffer
  buffer[buffer_size-1] = '\0';

  subject =(PCRE2_SPTR) buffer;
  subject_length = (PCRE2_SIZE)strlen((char *)subject);

  re = pcre2_compile(
      pattern,               /* the pattern */
      PCRE2_ZERO_TERMINATED, /* indicates pattern is zero-terminated */
      0,                     /* default options */
      &errornumber,          /* for error number */
      &erroroffset,          /* for error offset */
      NULL);                 /* use default compile context */

  if (re == NULL) { /* Compilation failed: print the error message and exit. */
    PCRE2_UCHAR buffer[256];
    pcre2_get_error_message(errornumber, buffer, sizeof(buffer));
    fprintf( stderr, "PCRE2 compilation failed at offset %d: %s\n", (int)erroroffset, buffer );
    return;
  }

  /* Now run the match. */
  match_data = pcre2_match_data_create_from_pattern(re, NULL);

  rc = pcre2_match(
      re,                   /* the compiled pattern */
      subject,              /* the subject string */
      subject_length,       /* the length of the subject */
      0,                    /* start at offset 0 in the subject */
      0,                    /* default options */
      match_data,           /* block for storing the result */
      NULL);                /* use default match context */

  if (rc < 0) { /* Matching failed: handle error cases */
    switch(rc) {
      case PCRE2_ERROR_NOMATCH: fprintf( stderr, "No match\n" ); break;
      default: fprintf( stderr, "Matching error %d\n", rc );     break;
    }
    pcre2_match_data_free(match_data);   /* Release memory used for the match */
    pcre2_code_free(re);                 /*   data and the compiled pattern. */
    return;
  }

  /* Match succeeded. Get a pointer to the output vector, where string offsets are stored. */
  ovector = pcre2_get_ovector_pointer(match_data);

  //
  //  the zeroth entry is the entire match; we could simply use it,
  //  but let's use some general parsing code starting at index 1
  //
  driver_parse[0] = '\0';
  char decimal_point[] = ".";
  for (int i = 1; i < rc; i++) {
    PCRE2_SPTR substring_start = subject + ovector[2*i];
    PCRE2_SIZE substring_length = ovector[2*i+1] - ovector[2*i];
    strncat( driver_parse, (char *)substring_start, (size_t)substring_length );
    if( i != rc-1 ) strncat( driver_parse, decimal_point, (size_t)1 );
  }

  pcre2_match_data_free(match_data);
  pcre2_code_free(re);
  return;
}




//
//  main program
//
//   collects info (via API or the above routine) and displays it
//
int
main ( int argc, char * argv[] )
{
  int  device, number_of_devices, version_int_code;
  cudaDeviceProp deviceProp;

  char parsed_string[1024];

  //
  //  API and CLI calls for software properties
  //

  //  CUDA version via the API
  cudaRuntimeGetVersion( &version_int_code );
  cudaErrorCheck();
  fprintf( stdout, "\n   CUDA library version:   %d.%d\n", version_int_code/1000, (version_int_code%1000)/10 );

  //  CUDA version from nvidia-smi
  memset( parsed_string, 0, (size_t)1024 );
  driver_version( parsed_string, 2 );
  fprintf( stdout, "    driver CUDA version:   %s\n", parsed_string );

  //  driver version from the system
  memset( parsed_string, 0, (size_t)1024 );
  driver_version( parsed_string, 1 );
  fprintf( stdout, "     GPU driver version: %s\n", parsed_string );

  //  nvcc version from the executable version string
  memset( parsed_string, 0, (size_t)1024 );
  driver_version( parsed_string, 3 );
  fprintf( stdout, "           nvcc version: %s\n", parsed_string );

  fprintf( stdout, " ---------------------------------------\n" );


  //
  //  device info
  //

  //  device count from the API
  cudaGetDeviceCount( &number_of_devices );
  cudaErrorCheck();

  //  loop over all devices
  for ( device = 0; device < number_of_devices; ++device ) {

    cudaGetDeviceProperties(&deviceProp, device);
    cudaErrorCheck();

    // https://docs.nvidia.com/cuda/cuda-runtime-api/structcudaDeviceProp.html
    fprintf( stdout, "\n =Properties of Device %d (PCI bus ID: %d)=\n %s (%s-GPU board)\n Compute Capability: %d.%d\n\n             Clock Rate: %5.2f GHz\n      Memory Clock Rate: %5.2f GHz\n       Memory Bus Width:  %4d bits\n%s Total (%sECC) Memory:  %4zu MB\n          L2 Cache size:%6d kB\n\n %5d multiprocessors\n %5d asynchronous engines\n %5d threads per block (max)\n %5d threads per multiprocessor (max)\n\n single-to-double performance ratio: %d\n\n",
           device,
           deviceProp.pciBusID,
           deviceProp.name,
           deviceProp.isMultiGpuBoard == 1 ? "multi" : "single",
	   deviceProp.major, deviceProp.minor,
	   deviceProp.clockRate/1.0E+06,
	   deviceProp.memoryClockRate/1.0E+06,
	   deviceProp.memoryBusWidth,
           deviceProp.ECCEnabled == 1 ? "    " : "", deviceProp.ECCEnabled == 1 ? "" : "non-",
	   deviceProp.totalGlobalMem/1048576,
	   deviceProp.l2CacheSize/1024,
	   deviceProp.multiProcessorCount,
           deviceProp.asyncEngineCount,
	   deviceProp.maxThreadsPerBlock,
	   deviceProp.maxThreadsPerMultiProcessor,
	   deviceProp.singleToDoublePrecisionPerfRatio );
  }

  return 0;
}

