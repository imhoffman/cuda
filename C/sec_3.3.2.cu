//  https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__MEMORY.html

#include<stdlib.h>
#include<cuda_runtime_api.h>

__global__
void compute_on_device ( float *base_addr, size_t pitch, int Nrows, int Ncols )
{
  return;
}


int
main ( void )
{
  int    height, width;
  float  *h_addr, *d_addr;
  size_t d_pitch;

  height = 128;
  width  =  16;

  h_addr =(float *) malloc( (size_t)height*width*sizeof(float) );

  //  `d_pitch` will be set to the pitch used by the device upon return
  //  size is computed on device as "width (in bytes) * height" so
  //  DON'T give _both_ `width` and `height` in bytes
  cudaMallocPitch( &d_addr, &d_pitch,
                    (size_t)height*sizeof(float), (size_t)width );

  compute_on_device<<<1,512>>>( d_addr, d_pitch, height, width );

  free    ( h_addr );
  cudaFree( d_addr );

  return 0;
}

