// compile with nvcc

#include<stdio.h>
#include<cuda_runtime_api.h>

int
main ( void )
{
  int            i, device_count;
  cudaDeviceProp device_properties;

  cudaGetDeviceCount( &device_count );

  printf( "\n There %s %d CUDA device%s available.\n\n",
      device_count != 1 ? "are" : "is",
      device_count,
      device_count != 1 ? "s" : "" );

  for ( i=0; i < device_count; i++ ) {
    cudaGetDeviceProperties( &device_properties, i );
    printf( " the device at bus ID %d can support %d threads per block\n\n",
        device_properties.pciBusID, device_properties.maxThreadsPerBlock );
  }

  return 0;
}

