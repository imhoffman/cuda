/*
 *  $ nvcc -ccbin=gcc-12 cuda_hello.cu && ./a.out 7 3 42 1
 *  
 *  be sure to set modest values for threads and threads/block
 */
#include<stdio.h>
#include<stdlib.h>


//
//  the kernel
//   simply spread around a shared value and print it
//
__global__
void
cuda_hello( const double a )
{

  //  share a value within each block
  __shared__ double block_wide_value;

  //  set a different shared value in each block;
  //   set it on only one of the threads, both as
  //   a demonstration, and to preclude a race
  if( threadIdx.x == blockDim.x-1 )
    block_wide_value = a * ( (double)blockIdx.x/(double)blockDim.x + 1.0 );

  //  wait for the writing to be done so that all
  //   threads will have the value available to read
  __syncthreads();

  //  every thread prints its info
  printf( "Hello from block %d, thread %d of %d, with shared value %9.6f !\n",
          blockIdx.x,
          threadIdx.x+1,
          blockDim.x,
          block_wide_value ) ;
}


/*
 * main
 *
 */
int
main( int argc, char *argv[] )
{
  int num_threads_per_block, num_blocks, user_seed;
  int num_devices, user_cuda_device=0;
  cudaError_t cuda_error;
  cudaDeviceProp deviceProp;

  //
  //  cli parser
  //
  if( argc <= 3 ) {
    fprintf( stderr, " usage: exe <blocks> <threads per block> <random seed> [<cuda device index>]\n" );
    return 1;
  } else {
    num_threads_per_block =(int) atol( argv[2] );
    num_blocks            =(int) atol( argv[1] );
    user_seed             =(int) atol( argv[3] );
    if( argc==5 ) user_cuda_device =(int) atol( argv[4] );
  }
  srand( user_seed );

  //
  //  cuda error handling
  //
  cuda_error = cudaGetDeviceCount( &num_devices ); 
  if( cuda_error != cudaSuccess ) {
    fprintf( stderr, " CUDA device query went wrong; cuda_error %d\n", cuda_error );
    return 3;
  }
  if( user_cuda_device < num_devices ) {
    cuda_error = cudaSetDevice( user_cuda_device );
    if( cuda_error != cudaSuccess ) {
      fprintf( stderr, " CUDA device setting went wrong; cuda_error %d\n", cuda_error );
      return 4;
    }
    cuda_error = cudaGetDeviceProperties(&deviceProp, user_cuda_device);
    if( cuda_error != cudaSuccess ) {
      fprintf( stderr, " CUDA device query went wrong; cuda_error %d\n", cuda_error );
      return 6;
    }
    fprintf( stdout, " running on %s\n", deviceProp.name );
  } else {
    fprintf( stderr, " you only have %d device%s, not %d\n",
                     num_devices,
                     num_devices>1 ? "s" : "",
                     user_cuda_device+1 );
    return 2;
  }

  /////////////////////////////////////////////////////////
  //
  //  made it past the error handling! launch the kernel
  //
  /////////////////////////////////////////////////////////
  cuda_hello<<< num_blocks, num_threads_per_block >>>( (double)rand() / (double)RAND_MAX );

  //
  //  the side-effects are in the kernel, but we must
  //   wait until after kernel return; even then the
  //   printf buffer will only flush if we sync
  //
  cudaDeviceSynchronize();
  //fflush( stdout );   //  this is not sufficient ...

  return 0;
}

