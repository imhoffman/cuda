// https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#device-enumeration
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<cuda.h>
#include<cuda_runtime_api.h>


//  subroutine to print CUDA core only for failures
//  as per 3.2.9
void
cudaErrorCheck ( void )
{
  int error_code = cudaPeekAtLastError();
  if ( error_code )
    fprintf( stderr, "\n latest CUDA error code: %d\n\n", error_code );
  
  return;
}


//  parse driver version out of system information via a pipe
//  there does not seem to be an API call for this
//  returning as a float saves us having to allocate
//   a char array in main; hopefully NVIDIA doesn't
//   reformat their driver identifiers
float
driver_version ( void )
{
  const char cmd[] = "cat /proc/driver/nvidia/version";
  const int buffer_size = 128;
  char  buffer[buffer_size];
  FILE  *cmd_fp;
  char  *token_ptr;
  float driver_parse = 0.0F;   //  default output if something goes wrong

  memset( buffer, 0, (size_t)buffer_size );
  if ( ( cmd_fp = popen( cmd, "r" ) ) != NULL )
    fgets( buffer, (size_t)buffer_size, cmd_fp );

  //  scroll through the buffer looking for the float-formatted version string
  //   I prefer explicit re-entry in my strtok
  if ( pclose( cmd_fp ) == 0 ) {
    token_ptr = buffer;
    while ( token_ptr != NULL )
      if ( ( sscanf( strtok_r( token_ptr, " ", &token_ptr ), "%f", &driver_parse ) ) == 1 )
        break;
  }

  return driver_parse;
}


//  parse nvcc version out of system information via a pipe
//  there does not seem to be an API call for this
//  main has a 16-byte allocation for this return
char *
nvcc_version ( void )
{
  const char cmd[] = "/usr/local/cuda/bin/nvcc --version";
  const char search_str_1[] = ", V";
  const char search_str_2[] = "\n";
  const int read_buffer_size = 80, write_buffer_size = 1024;
  char  read_buffer[read_buffer_size], write_buffer[write_buffer_size];
  FILE  *cmd_fp;
  char  *out_str;

  //  concatenate the entire multi-line output
  memset(  read_buffer, 0, (size_t)read_buffer_size );
  memset( write_buffer, 0, (size_t)write_buffer_size );
  if ( ( cmd_fp = popen( cmd, "r" ) ) != NULL )
    while( ( fgets( read_buffer, (size_t)read_buffer_size, cmd_fp ) ) != NULL ) {
      sprintf( write_buffer+strlen(write_buffer), "%s", read_buffer );
      memset(  read_buffer, 0, (size_t)read_buffer_size );
    }

  //  point to the value of interest and null-terminate
  if ( pclose( cmd_fp ) == 0 ) {
    out_str = strstr( write_buffer, search_str_1 ) + strlen(search_str_1);
    memset( strstr( out_str, search_str_2 ), 0, sizeof(char) );
  } else {
    out_str =(char *) cmd;
  }

  return out_str;
}


//
//  main program
//
int
main ( void )
{
  int  device, number_of_devices, version_int_code;
  char nvcc_ver[16];
  cudaDeviceProp deviceProp;


  //
  //  begin collecting software info
  //

  //  CUDA version via the API
  cudaRuntimeGetVersion( &version_int_code );
  cudaErrorCheck();
  fprintf( stdout, "\n           CUDA version:   %d.%d\n", version_int_code/1000, (version_int_code%1000)/10 );

  //  driver version from the system
  fprintf( stdout, "         driver version:  %6.2f\n", driver_version() );

  //  nvcc version from the system
  memcpy( nvcc_ver, nvcc_version(), (size_t)16 );
  fprintf( stdout, "           nvcc version: %s\n", nvcc_ver );

  fprintf( stdout, " ---------------------------------------\n" );


  //
  //  begin collecting device info
  //

  //  device count from the API
  cudaGetDeviceCount( &number_of_devices );
  cudaErrorCheck();

  //  loop over all devices
  for ( device = 0; device < number_of_devices; ++device ) {

    cudaGetDeviceProperties(&deviceProp, device);
    cudaErrorCheck();

    // https://docs.nvidia.com/cuda/cuda-runtime-api/structcudaDeviceProp.html
    fprintf( stdout, "\n =Properties of Device %d (PCI bus ID: %d)=\n %s (%s-GPU board)\n Compute Capability: %d.%d\n\n             Clock Rate: %5.2f GHz\n      Memory Clock Rate: %5.2f GHz\n       Memory Bus Width:  %4d bits\n%s Total (%sECC) Memory:  %4zu MB\n          L2 Cache size:%6d kB\n\n %5d multiprocessors\n %5d asynchronous engines\n %5d threads per block (max)\n %5d threads per multiprocessor (max)\n\n single-to-double performance ratio: %d\n\n",
           device,
           deviceProp.pciBusID,
           deviceProp.name,
           deviceProp.isMultiGpuBoard == 1 ? "multi" : "single",
	   deviceProp.major, deviceProp.minor,
	   deviceProp.clockRate/1.0E+06,
	   deviceProp.memoryClockRate/1.0E+06,
	   deviceProp.memoryBusWidth,
           deviceProp.ECCEnabled == 1 ? "    " : "", deviceProp.ECCEnabled == 1 ? "" : "non-",
	   deviceProp.totalGlobalMem/1048576,
	   deviceProp.l2CacheSize/1024,
	   deviceProp.multiProcessorCount,
           deviceProp.asyncEngineCount,
	   deviceProp.maxThreadsPerBlock,
	   deviceProp.maxThreadsPerMultiProcessor,
	   deviceProp.singleToDoublePrecisionPerfRatio );
  }

  return 0;
}

