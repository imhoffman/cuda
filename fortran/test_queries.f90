
 program test_queries
  use cudafor

  integer :: Ndev, devnum, error = 0
  type( cudadeviceprop ) :: deviceProp

  error = error + cudaGetDeviceCount( Ndev )

  100 format( /A,I0,A,I0,A/,X,A,X,A,A/,A,I0,A,I0/,/A,F5.2,A/,A,F5.2,A/,A,I4,A/,A,I5,A/, &
    A,I5,A/,/X,I5,A/,X,I5,A/,X,I5,A/,X,I5,A/,/A,I0/ )
  do devnum = 0, Ndev-1
    error = error + cudaGetDeviceProperties( deviceProp, 0 )
    write( 6, 100 ) &
      " =Properties of Device ", devnum, " (PCI bus ID: ", deviceProp%pciBusID, ")=", &
      trim(deviceProp%name), trim( merge( "(multi ", "(single ", &
        deviceProp%isMultiGpuBoard .eq. 1 ) ), "-GPU board)", &
      " Compute Capability: ", deviceProp%major,".",deviceProp%minor, &
      "             Clock Rate: ", real(deviceProp%ClockRate)/1.0E+06, " GHz", &
      "      Memory Clock Rate: ", real(deviceProp%memoryClockRate)/1.0E+06, " GHz", &
      "       Memory Bus Width:  ", deviceProp%memoryBusWidth, " bits", &
      merge( "    Total (ECC) Memory:  ", " Total (non-ECC) Memory:  ", &
        deviceProp%ECCEnabled .eq. 1 ), & 
      deviceProp%totalGlobalMem/1048576, " MB", &
      "          L2 Cache size: ", deviceProp%l2CacheSize/1024, " kB", &
      deviceProp%multiProcessorCount, " multiprocessors", &
      deviceProp%asyncEngineCount, " asynchronous engines", &
      deviceProp%maxThreadsPerBlock, " threads per block (max)", &
      deviceProp%maxThreadsPerMultiProcessor, " threads per multiprocessor (max)", &
      " single-to-double performance ratio: ", deviceProp%singleToDoublePrecisionPerfRatio
  enddo

  if ( error .ne. 0 ) then
    write(6,'(/X,A,I0,A/)') "There were errors! (", error, " of them"
  endif

 end program test_queries


